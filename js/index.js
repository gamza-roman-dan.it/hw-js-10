"use strict"

//1-
// способи. createElement() || insertAdjacentHTML()
//2-
//яб спочатку знайшовби той клас, опісля й видаливби, наприклад:
//const class = document.querySelector(".navigations");
//class.classList.remove("navigations");
//3-
//для createElement() використовуємо = append(), prepend(), before(), after()
//для insertAdjacentHTML() використовуємо = beforeBegin, afterBegin, beforeEnd, afterEnd

//__________________________________________________________________________________________

//1-
const tagA = document.createElement("a");
tagA.innerText = "Learn More";
tagA.href = "#";

const footerP = document.querySelector("footer p");
footerP.after(tagA);

//2-
const tagSelect = document.createElement("select");
tagSelect.name = "rating";
tagSelect.id = "rating";

const featuresClass = document.querySelector(".features");
featuresClass.before(tagSelect);

for (let i = 4; i > 0; i--) {
    if (i === 1){
        tagSelect.insertAdjacentHTML('beforeend', `<option value='1'>1 star</option>`)
    }
    else {tagSelect.insertAdjacentHTML("beforeend", `<option value='${i}'>${i} stars</option>`)
    }
}